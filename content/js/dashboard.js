/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.9333981841763943, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.29352997145575643, 500, 1500, "getCountCheckInOutChildren"], "isController": false}, {"data": [0.9999016570782318, 500, 1500, "getLatestMobileVersion"], "isController": false}, {"data": [0.9879576107899807, 500, 1500, "findAllConfigByCategory"], "isController": false}, {"data": [0.9152625152625152, 500, 1500, "getNotifications"], "isController": false}, {"data": [0.12439903846153846, 500, 1500, "getHomefeed"], "isController": false}, {"data": [0.9530539632338407, 500, 1500, "me"], "isController": false}, {"data": [0.9561554662071606, 500, 1500, "findAllChildrenByParent"], "isController": false}, {"data": [0.8433462532299741, 500, 1500, "getAllClassInfo"], "isController": false}, {"data": [0.9809196121363779, 500, 1500, "findAllSchoolConfig"], "isController": false}, {"data": [0.005672609400324149, 500, 1500, "getChildCheckInCheckOut"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 53970, 0, 0.0, 277.1156939040207, 11, 5820, 168.0, 491.90000000000146, 968.0, 2154.9900000000016, 179.3350944524747, 892.5642786420692, 246.90152452067653], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["getCountCheckInOutChildren", 1051, 0, 0.0, 1426.2997145575632, 563, 2683, 1412.0, 1888.8000000000002, 2014.8, 2286.4, 3.4959103503560773, 2.2088417936331854, 4.325506263575342], "isController": false}, {"data": ["getLatestMobileVersion", 20337, 0, 0.0, 73.17986920391408, 11, 1185, 42.0, 172.0, 202.0, 264.9900000000016, 67.79949192887004, 45.354152315699196, 49.98888320927429], "isController": false}, {"data": ["findAllConfigByCategory", 7266, 0, 0.0, 205.55188549408203, 33, 1203, 183.0, 331.3000000000002, 409.64999999999964, 649.9799999999996, 24.214672771990163, 27.383389716762316, 31.450698033932536], "isController": false}, {"data": ["getNotifications", 4095, 0, 0.0, 365.246886446887, 66, 1844, 297.0, 691.4000000000001, 899.3999999999996, 1263.2799999999997, 13.631461212750658, 113.32483330483042, 15.162338204416992], "isController": false}, {"data": ["getHomefeed", 832, 0, 0.0, 1803.847355769232, 573, 5820, 1783.0, 2290.4, 2430.0999999999995, 4177.939999999997, 2.764624765322567, 33.68576484075163, 13.833923142102378], "isController": false}, {"data": ["me", 5059, 0, 0.0, 295.4485076101996, 64, 1571, 249.0, 486.0, 660.0, 945.1999999999989, 16.860691824947423, 22.19886918236976, 53.43061032417421], "isController": false}, {"data": ["findAllChildrenByParent", 5223, 0, 0.0, 286.2012253494167, 53, 1375, 236.0, 469.0, 713.8000000000002, 1011.0, 17.400835559938432, 19.541953997977732, 27.800553687557883], "isController": false}, {"data": ["getAllClassInfo", 3096, 0, 0.0, 483.66892764857823, 77, 1970, 401.5, 891.6000000000004, 1091.7499999999982, 1406.0, 10.304097343100482, 29.412614344361867, 26.45456241700309], "isController": false}, {"data": ["findAllSchoolConfig", 6394, 0, 0.0, 233.62730685017237, 41, 1470, 207.0, 370.0, 457.0, 744.1500000000005, 21.308929487905832, 464.7177864491338, 15.627935591227812], "isController": false}, {"data": ["getChildCheckInCheckOut", 617, 0, 0.0, 2427.63695299838, 1388, 3825, 2389.0, 3010.6000000000004, 3268.7000000000003, 3472.3000000000006, 2.052827878533808, 136.87189785829665, 9.44621578481573], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 53970, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
